/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.dto;

import cz.cvut.kbss.reporting.environment.util.Generator;
import cz.cvut.kbss.reporting.util.Constants;
import org.junit.Test;

import static org.junit.Assert.assertNotEquals;

public class ReportRevisionInfoTest {

    @Test
    public void twoRevisionInfosWithSameRevisionButDifferentReportsAreDifferent() {
        final ReportRevisionInfo first = new ReportRevisionInfo();
        first.setId(Generator.randomInt());
        first.setRevision(Constants.INITIAL_REVISION);

        final ReportRevisionInfo second = new ReportRevisionInfo();
        second.setId(Generator.randomInt());
        second.setRevision(Constants.INITIAL_REVISION);
        assertNotEquals(first, second);
        assertNotEquals(first.hashCode(), second.hashCode());
    }
}