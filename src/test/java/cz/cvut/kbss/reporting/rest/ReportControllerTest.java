/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import cz.cvut.kbss.reporting.dto.OccurrenceReportDto;
import cz.cvut.kbss.reporting.dto.ReportRevisionInfo;
import cz.cvut.kbss.reporting.dto.reportlist.ReportDto;
import cz.cvut.kbss.reporting.environment.config.MockServiceConfig;
import cz.cvut.kbss.reporting.environment.util.Environment;
import cz.cvut.kbss.reporting.environment.util.Generator;
import cz.cvut.kbss.reporting.environment.util.ReportRevisionComparator;
import cz.cvut.kbss.reporting.exception.NotFoundException;
import cz.cvut.kbss.reporting.exception.PersistenceException;
import cz.cvut.kbss.reporting.exception.ValidationException;
import cz.cvut.kbss.reporting.model.OccurrenceReport;
import cz.cvut.kbss.reporting.model.Person;
import cz.cvut.kbss.reporting.rest.dto.mapper.DtoMapper;
import cz.cvut.kbss.reporting.rest.handler.ErrorInfo;
import cz.cvut.kbss.reporting.service.OccurrenceReportService;
import cz.cvut.kbss.reporting.util.IdentificationUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MvcResult;

import javax.persistence.RollbackException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

;

@ContextConfiguration(classes = {MockServiceConfig.class})
public class ReportControllerTest extends BaseControllerTestRunner {

    private static final String REPORTS_PATH = "/reports/";

    @Autowired
    private OccurrenceReportService reportServiceMock;

    @Autowired
    private DtoMapper mapper;

    private Person author;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        Mockito.reset(reportServiceMock);
        this.author = Generator.getPerson();
        Environment.setCurrentUser(author);
    }

    @Test
    public void getAllReportsReturnsEmptyCollectionWhenThereAreNoReports() throws Exception {
        when(reportServiceMock.findAll()).thenReturn(Collections.emptyList());
        final MvcResult result = mockMvc.perform(get("/reports").accept(MediaType.APPLICATION_JSON_VALUE))
                                        .andExpect(status().isOk()).andReturn();
        final List<ReportDto> res = objectMapper
                .readValue(result.getResponse().getContentAsByteArray(), new TypeReference<List<ReportDto>>() {
                });
        assertNotNull(res);
        assertTrue(res.isEmpty());
    }

    @Test
    public void getReportReturnsNotFoundForUnknownId() throws Exception {
        final Integer unknownId = 117;
        when(reportServiceMock.find(unknownId)).thenReturn(null);
        mockMvc.perform(get(REPORTS_PATH + unknownId)).andExpect(status().isNotFound());
    }

    @Test
    public void testGetLatestRevision() throws Exception {
        final OccurrenceReport latestRevision = Generator.generateOccurrenceReport(true);
        latestRevision.setRevision(Generator.randomInt(10));
        when(reportServiceMock.findLatestRevision(latestRevision.getFileNumber())).thenReturn(latestRevision);
        final MvcResult result = mockMvc.perform(get(REPORTS_PATH + "chain/" + latestRevision.getFileNumber()))
                                        .andExpect(status().isOk()).andReturn();
        final OccurrenceReportDto res = readValue(result, OccurrenceReportDto.class);
        assertNotNull(res);
        assertEquals(latestRevision.getId(), res.getId());
        assertEquals(latestRevision.getRevision(), res.getRevision());
    }

    @Test
    public void getLatestRevisionThrowsNotFoundWhenReportChainIsNotFound() throws Exception {
        final Long fileNumber = 12345L;
        when(reportServiceMock.findLatestRevision(fileNumber)).thenReturn(null);
        mockMvc.perform(get(REPORTS_PATH + "chain/" + fileNumber)).andExpect(status().isNotFound());
    }

    @Test
    public void testGetReportChainRevisions() throws Exception {
        final List<OccurrenceReport> chain = Generator.generateOccurrenceReportChain(author, true);
        Collections.sort(chain, new ReportRevisionComparator<>());  // sort by revision descending
        final Long fileNumber = chain.get(0).getFileNumber();
        final List<ReportRevisionInfo> revisions = new ArrayList<>(chain.size());
        for (int i = 0; i < chain.size(); i++) {
            final OccurrenceReport r = chain.get(i);
            r.setId(i);
            final ReportRevisionInfo revision = new ReportRevisionInfo();
            revision.setId(r.getId());
            revision.setRevision(r.getRevision());
            revision.setCreated(r.getDateCreated());
            revisions.add(revision);
        }
        when(reportServiceMock.getReportChainRevisions(fileNumber)).thenReturn(revisions);
        final MvcResult result = mockMvc.perform(get(REPORTS_PATH + "chain/" + fileNumber + "/revisions"))
                                        .andExpect(status().isOk()).andReturn();
        final List<ReportRevisionInfo> res = readValue(result, new TypeReference<List<ReportRevisionInfo>>() {
        });
        assertNotNull(res);
        assertEquals(revisions, res);
    }

    @Test
    public void getReportChainRevisionsThrowsNotFoundForUnknownReportChainIdentifier() throws Exception {
        final Long fileNumber = Long.MAX_VALUE;
        when(reportServiceMock.getReportChainRevisions(fileNumber)).thenReturn(Collections.emptyList());
        mockMvc.perform(get(REPORTS_PATH + "chain/" + fileNumber + "/revisions")).andExpect(status().isNotFound());
        verify(reportServiceMock).getReportChainRevisions(fileNumber);
    }

    @Test
    public void testGetOccurrenceReportRevisionByChainIdentifierAndRevisionNumber() throws Exception {
        final List<OccurrenceReport> chain = Generator.generateOccurrenceReportChain(author, true);
        chain.forEach(r -> {
            r.setId(Generator.randomInt());
        });
        final OccurrenceReport report = chain.get(Generator.randomInt(chain.size()) - 1);
        when(reportServiceMock.findRevision(report.getFileNumber(), report.getRevision())).thenReturn(report);
        final MvcResult result = mockMvc
                .perform(get(REPORTS_PATH + "chain/" + report.getFileNumber() + "/revisions/" + report.getRevision()))
                .andExpect(status().isOk()).andReturn();
        final OccurrenceReportDto res = readValue(result, OccurrenceReportDto.class);
        assertNotNull(res);
        assertEquals(report.getFileNumber(), res.getFileNumber());
        assertEquals(report.getRevision(), res.getRevision());
        assertEquals(report.getId(), res.getId());
    }

    @Test
    public void getRevisionThrowsNotFoundWhenRevisionIsNotFound() throws Exception {
        final Long fileNumber = 12345L;
        final Integer revision = 3;
        when(reportServiceMock.findRevision(fileNumber, revision)).thenReturn(null);

        mockMvc.perform(get(REPORTS_PATH + "chain/" + fileNumber + "/revisions/" + revision))
               .andExpect(status().isNotFound());
    }

    @Test
    public void createReportReturnsLocationOfNewInstance() throws Exception {
        final OccurrenceReport report = Generator.generateOccurrenceReport(false);
        final Integer key = 117;
        doAnswer(call -> {
            final OccurrenceReport r = (OccurrenceReport) call.getArguments()[0];
            r.setId(key);
            return null;
        }).when(reportServiceMock).persist(any(OccurrenceReport.class));

        final MvcResult result = mockMvc
                .perform(post("/reports").content(toJson(mapper.occurrenceReportToOccurrenceReportDto(report)))
                                         .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        verifyLocationEquals(REPORTS_PATH + key, result);
        verify(reportServiceMock).persist(any(OccurrenceReport.class));
    }

    @Test
    public void createReportReturnsValidationExceptionThrownByServiceAsResponse() throws Exception {
        final OccurrenceReport report = Generator.generateOccurrenceReport(false);
        doThrow(new ValidationException("Invalid report.")).when(reportServiceMock)
                                                           .persist(any(OccurrenceReport.class));
        mockMvc.perform(post("/reports").content(toJson(mapper.occurrenceReportToOccurrenceReportDto(report)))
                                        .contentType(MediaType.APPLICATION_JSON_VALUE))
               .andExpect(status().isConflict());
        verify(reportServiceMock).persist(any(OccurrenceReport.class));
    }

    @Test
    public void createNewRevisionReturnsLocationOfNewRevision() throws Exception {
        final List<OccurrenceReport> chain = Generator.generateOccurrenceReportChain(author, true);
        final Long fileNumber = chain.get(0).getFileNumber();
        Collections.sort(chain, new ReportRevisionComparator<>());  // Sort descending
        final OccurrenceReport newRevision = new OccurrenceReport();
        newRevision.setId(Generator.randomInt());
        newRevision.setFileNumber(fileNumber);
        newRevision.setRevision(chain.get(0).getRevision() + 1);
        when(reportServiceMock.createNewRevision(fileNumber)).thenReturn(newRevision);

        final MvcResult result = mockMvc.perform(post(REPORTS_PATH + "chain/" + fileNumber + "/revisions"))
                                        .andExpect(status().isCreated()).andReturn();
        verifyLocationEquals(REPORTS_PATH + newRevision.getId(), result);
    }

    @Test
    public void createNewRevisionReturnsNotFoundThrownByServiceAsResponse() throws Exception {
        final Long fileNumber = Long.MAX_VALUE;
        when(reportServiceMock.createNewRevision(fileNumber))
                .thenThrow(NotFoundException.create("Report chain", fileNumber));
        mockMvc.perform(post(REPORTS_PATH + "chain/" + fileNumber + "/revisions")).andExpect(status().isNotFound());
        verify(reportServiceMock).createNewRevision(fileNumber);
    }

    @Test
    public void updateReportPassesReportToServiceForUpdate() throws Exception {
        final OccurrenceReport report = prepareReport();
        mockMvc.perform(
                put(REPORTS_PATH + report.getId())
                        .content(toJson(mapper.occurrenceReportToOccurrenceReportDto(report)))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
               .andExpect(status().isNoContent());
        final ArgumentCaptor<OccurrenceReport> captor = ArgumentCaptor.forClass(OccurrenceReport.class);
        verify(reportServiceMock).update(captor.capture());
        final OccurrenceReport argument = captor.getValue();
        assertNotNull(argument);
        assertEquals(report.getId(), argument.getId());
    }

    private OccurrenceReport prepareReport() {
        final OccurrenceReport report = Generator.generateOccurrenceReport(false);
        report.setId(Generator.randomInt());
        when(reportServiceMock.find(report.getId())).thenReturn(report);
        return report;
    }

    @Test
    public void updateReportThrowsBadRequestWhenKeyInPathDoesNotMatchReportKey() throws Exception {
        final OccurrenceReport report = prepareReport();
        final Integer otherKey = 117;
        mockMvc.perform(
                put(REPORTS_PATH + otherKey).content(toJson(mapper.occurrenceReportToOccurrenceReportDto(report)))
                                            .contentType(MediaType.APPLICATION_JSON_VALUE))
               .andExpect(status().isBadRequest());
    }

    @Test
    public void updateReportThrowsNotFoundForUnknownReportId() throws Exception {
        final OccurrenceReport report = Generator.generateOccurrenceReport(false);
        report.setId(Generator.randomInt());
        when(reportServiceMock.find(report.getId())).thenReturn(null);
        mockMvc.perform(
                put(REPORTS_PATH + report.getId())
                        .content(toJson(mapper.occurrenceReportToOccurrenceReportDto(report)))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
               .andExpect(status().isNotFound());
        verify(reportServiceMock, never()).update(any(OccurrenceReport.class));
    }

    @Test
    public void updateReportReturnsValidationExceptionAsResponseWhenUpdateValidationFails() throws Exception {
        final OccurrenceReport report = prepareReport();
        doThrow(new ValidationException("Invalid report.")).when(reportServiceMock).update(any(OccurrenceReport.class));
        mockMvc.perform(
                put(REPORTS_PATH + report.getId())
                        .content(toJson(mapper.occurrenceReportToOccurrenceReportDto(report)))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
               .andExpect(status().isConflict());
    }

    @Test
    public void transitionToNextPhaseCallsService() throws Exception {
        final OccurrenceReport report = prepareReport();
        mockMvc.perform(put(REPORTS_PATH + report.getId() + "/phase")).andExpect(status().isNoContent());
        verify(reportServiceMock).transitionToNextPhase(report);
    }

    @Test
    public void removeChainCallsService() throws Exception {
        final Long fileNumber = IdentificationUtils.generateFileNumber();
        mockMvc.perform(delete(REPORTS_PATH + "chain/" + fileNumber)).andExpect(status().isNoContent());
        verify(reportServiceMock).removeReportChain(fileNumber);
    }

    @Test
    public void persistenceExceptionIsWrappedInJsonObjectWithReadableMessage() throws Exception {
        final OccurrenceReport report = prepareReport();
        final String message = "Expected some value in attribute blabla, but found none.";
        Mockito.doThrow(new PersistenceException(new RollbackException(message))).when(reportServiceMock)
               .persist(any(OccurrenceReport.class));
        final MvcResult result = mockMvc.perform(
                post(REPORTS_PATH).content(toJson(mapper.occurrenceReportToOccurrenceReportDto(report)))
                                  .contentType(MediaType.APPLICATION_JSON_VALUE))
                                        .andExpect(status().isInternalServerError()).andReturn();
        final ErrorInfo errorInfo = readValue(result, ErrorInfo.class);
        assertEquals(message, errorInfo.getMessage());
    }
}