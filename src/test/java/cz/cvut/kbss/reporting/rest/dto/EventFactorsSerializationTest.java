/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.rest.dto;

import cz.cvut.kbss.reporting.dto.event.FactorGraph;
import cz.cvut.kbss.reporting.dto.event.FactorGraphEdge;
import cz.cvut.kbss.reporting.environment.util.Environment;
import cz.cvut.kbss.reporting.environment.util.Generator;
import cz.cvut.kbss.reporting.model.*;
import cz.cvut.kbss.reporting.rest.dto.mapper.DtoMapper;
import cz.cvut.kbss.reporting.rest.dto.mapper.DtoMapperImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

/**
 * These tests verify correct serialization of event (or occurrence) structure serialization.
 * <p>
 * I.e. mainly correct usage of reference ids when serializing graphs with loops.
 */
public class EventFactorsSerializationTest {

    private static final String HAS_PART_URI = Vocabulary.s_p_has_part;

    private DtoMapper dtoMapper;

    @Before
    public void setUp() {
        this.dtoMapper = new DtoMapperImpl();
    }

    @Test
    public void testSerializationOfOccurrenceWithSubEvents() throws Exception {
        final Occurrence occurrence = generateOccurrenceWithSubEvents();
        final FactorGraph container = dtoMapper.occurrenceToFactorGraph(occurrence);
        verifyStructure(occurrence, container);
    }

    public static Occurrence generateOccurrenceWithSubEvents() {
        final Occurrence occurrence = occurrence();
        occurrence.setChildren(new ArrayList<>());
        final int maxDepth = Generator.randomInt(5);
        for (int i = 0; i < Generator.randomInt(5); i++) {
            occurrence.getChildren().add(generateSubEvents(0, maxDepth));
        }
        return occurrence;
    }

    private static Occurrence occurrence() {
        final Occurrence occurrence = new Occurrence();
        occurrence.setName("Test");
        occurrence.setId(Generator.randomInt());
        return occurrence;
    }

    private static Event generateSubEvents(int currentDepth, final int maxDepth) {
        final Event evt = event();
        if (currentDepth < maxDepth) {
            evt.setChildren(new ArrayList<>());
            currentDepth++;
            for (int i = 0; i < Generator.randomInt(5); i++) {
                evt.getChildren().add(generateSubEvents(currentDepth, maxDepth));
            }
        }
        return evt;
    }

    private static Event event() {
        final Event evt = new Event();
        evt.setId(Generator.randomInt());
        return evt;
    }

    private void verifyStructure(Occurrence occurrence, FactorGraph graph) {
        final Map<Integer, Event> instanceMap = new HashMap<>();
        graph.getNodes().forEach(n -> instanceMap.put(n.getId(), n));
        final Set<Integer> visited = new HashSet<>();
        visited.add(occurrence.getId());

        verifyFactors(occurrence, occurrence.getFactors(), graph, instanceMap, visited);
        if (occurrence.getChildren() != null) {
            occurrence.getChildren().forEach(child -> {
                verifyEdge(instanceMap.get(occurrence.getId()), instanceMap.get(child.getId()), HAS_PART_URI, graph);
                verifyStructure(child, graph, instanceMap, visited);
            });
        }
        assertEquals(graph.getNodes().size(), visited.size());
    }

    private void verifyEdge(Event start, Event end, String type, FactorGraph graph) {
        final FactorGraphEdge edge = new FactorGraphEdge(start.getReferenceId(), end.getReferenceId(), type);
        assertTrue(graph.getEdges().contains(edge));
    }

    private void verifyStructure(Event event, FactorGraph graph, Map<Integer, Event> instanceMap,
                                 Set<Integer> visited) {
        if (visited.contains(event.getId())) {
            return;
        }
        visited.add(event.getId());
        verifyFactors(event, event.getFactors(), graph, instanceMap, visited);
        if (event.getChildren() != null) {
            event.getChildren().forEach(child -> {
                verifyEdge(instanceMap.get(event.getId()), instanceMap.get(child.getId()), HAS_PART_URI, graph);
                verifyStructure(child, graph, instanceMap, visited);
            });
        }
    }

    private void verifyFactors(AbstractEntity target, Set<Factor> factors, FactorGraph graph,
                               Map<Integer, Event> instanceMap,
                               Set<Integer> visited) {
        if (factors == null || factors.isEmpty()) {
            return;
        }
        for (Factor f : factors) {
            assert f.getTypes().size() == 1;
            verifyEdge(instanceMap.get(f.getEvent().getId()), instanceMap.get(target.getId()),
                    f.getTypes().iterator().next(), graph);
            verifyStructure(f.getEvent(), graph, instanceMap, visited);
        }
    }

    @Test
    public void testSerializationOfLinksBetweenOccurrenceAndEventsAtSameLevel() throws Exception {
        final Occurrence occurrence = generateOccurrenceWithLinkChainOnSameLevel();
        final FactorGraph container = dtoMapper.occurrenceToFactorGraph(occurrence);
        verifyStructure(occurrence, container);
    }

    private Occurrence generateOccurrenceWithLinkChainOnSameLevel() {
        final Occurrence occurrence = occurrence();
        occurrence.setFactors(new HashSet<>());
        final int chainLength = Generator.randomInt(5);
        for (int i = 0; i < Generator.randomInt(5); i++) {
            occurrence.getFactors().add(generateChainItem(0, chainLength));
        }
        return occurrence;
    }

    private Factor generateChainItem(int currentIndex, final int maxIndex) {
        final Factor f = new Factor();
        f.addType(Generator.randomFactorType());
        f.setEvent(event());
        if (currentIndex < maxIndex) {
            f.getEvent().setFactors(new HashSet<>());
            currentIndex++;
            for (int i = 0; i < Generator.randomInt(5); i++) {
                f.getEvent().getFactors().add(generateChainItem(currentIndex, maxIndex));
            }
        }
        return f;
    }

    @Test
    public void testSerializationOfOccurrenceWithSubEventsConnectedByFactors() throws Exception {
        final Occurrence occurrence = generateOccurrenceWithSubEvents();
        addFactorsToStructure(occurrence.getChildren());
        final FactorGraph graph = dtoMapper.occurrenceToFactorGraph(occurrence);

        verifyStructure(occurrence, graph);
    }

    private void addFactorsToStructure(List<Event> siblings) {
        if (siblings.size() >= 2) {
            for (int i = 0; i < Generator.randomInt(siblings.size()); i++) {
                final Event from = siblings.get(Generator.randomIndex(siblings));
                final Event to = siblings.get(Generator.randomIndex(siblings));
                if (from == to) {
                    continue;
                }
                if (to.getFactors() == null) {
                    to.setFactors(new HashSet<>());
                }
                final Factor f = new Factor();
                f.setEvent(from);
                f.addType(Generator.randomFactorType());
                to.getFactors().add(f);
            }
        }
        siblings.stream().filter(e -> e.getChildren() != null).forEach(e -> addFactorsToStructure(e.getChildren()));
    }

    @Test
    public void testSerializationOfOccurrenceWithFactorsConnectingEventsFromDifferentSubtrees() {
        final Occurrence occurrence = generateOccurrenceWithSubEvents();
        addCrossSubtreeFactors(occurrence.getChildren());
        final FactorGraph graph = dtoMapper.occurrenceToFactorGraph(occurrence);
        verifyStructure(occurrence, graph);
    }

    private void addCrossSubtreeFactors(List<Event> siblings) {
        if (siblings.size() >= 2) {
            final Event e1 = siblings.get(Generator.randomIndex(siblings));
            final Event e2 = siblings.get(Generator.randomIndex(siblings));
            if (e1 != e2 && e1.getChildren() != null && e1.getChildren().size() >= 2 && e2.getChildren() != null &&
                    e2.getChildren().size() >= 2) {
                final List<Event> e1Children = new ArrayList<>(e1.getChildren());
                final List<Event> e2Children = new ArrayList<>(e2.getChildren());
                final Event from = e1Children.get(Generator.randomIndex(e1Children));
                final Event to = e2Children.get(Generator.randomIndex(e2Children));
                final Factor f = new Factor();
                f.addType(Generator.randomFactorType());
                f.setEvent(from);
                to.addFactor(f);
            }
        }
        siblings.stream().filter(e -> e.getChildren() != null).forEach(e -> addCrossSubtreeFactors(e.getChildren()));
    }


    /**
     * Tree structure:
     * <pre>
     *     {@code
     *           1
     *       2 | 3 | 4
     *     5 6 | 7 | 8 9 10
     *         | 11|
     *     }
     * </pre>
     */
    @Test
    public void testDeserializationOfOccurrenceWithSubEvents() throws Exception {
        final FactorGraph graph = loadGraph("data/occurrenceWithSubEvents.json");
        final Occurrence res = dtoMapper.factorGraphToOccurrence(graph);
        final Map<Integer, Collection<Integer>> expectedChildren = new HashMap<>();
        expectedChildren.put(1, Arrays.asList(2, 3, 4));
        expectedChildren.put(2, Arrays.asList(5, 6));
        expectedChildren.put(3, Collections.singletonList(7));
        expectedChildren.put(4, Arrays.asList(8, 9, 10));
        expectedChildren.put(7, Collections.singletonList(11));
        verifyDeserializedTree(graph, res, expectedChildren);
    }

    private void verifyDeserializedTree(FactorGraph graph, Occurrence o,
                                        Map<Integer, Collection<Integer>> expectedChildren) {
        final Map<Integer, Event> dtos = new HashMap<>();
        graph.getNodes().forEach(n -> dtos.put(n.getId(), n));
        verifyChildren(o.getChildren(), expectedChildren.get(dtos.get(o.getId()).getReferenceId()), dtos);
        o.getChildren().forEach(c -> verifyDeserializedTree(c, expectedChildren, dtos));
    }

    private void verifyChildren(List<Event> children, Collection<Integer> expectedChildren, Map<Integer, Event> dtos) {
        assertEquals(expectedChildren.size(), children.size());
        final Map<Integer, Event> childMap = new HashMap<>();
        children.forEach(c -> childMap.put(c.getId(), c));
        for (Integer exp : expectedChildren) {
            assertTrue(childMap.containsKey(exp));
            final Event e = childMap.get(exp);
            final Event dto = dtos.get(e.getId());
            assertEquals(dto.getEventType(), e.getEventType());
            assertEquals(dto.getId(), e.getId());
        }
    }

    private void verifyDeserializedTree(Event e, Map<Integer, Collection<Integer>> expectedChildren,
                                        Map<Integer, Event> dtos) {
        if (e.getChildren() == null || e.getChildren().isEmpty()) {
            assertNull(expectedChildren.get(dtos.get(e.getId()).getReferenceId()));
            return;
        }
        verifyChildren(e.getChildren(), expectedChildren.get(dtos.get(e.getId()).getReferenceId()), dtos);
        e.getChildren().forEach(c -> verifyDeserializedTree(c, expectedChildren, dtos));
    }

    /**
     * Tree structure:
     * <pre>
     *     {@code
     *           1
     *       2 | 3 | 4
     *     5 6 | 7 |
     *     }
     * </pre>
     * Links:
     * <pre>
     * <ul>
     *     <li>2 -> 3, causes</li>
     *     <li>4 -> 3, mitigates</li>
     *     <li>5 -> 7, causes</li>
     * </ul>
     * </pre>
     */
    @Test
    public void testDeserializationOfOccurrenceWithSubEventsWithFactors() throws Exception {
        final FactorGraph graph = loadGraph("data/occurrenceWithSubEventsConnectedByFactors.json");
        final Occurrence res = dtoMapper.factorGraphToOccurrence(graph);
        final Map<Integer, Collection<Integer>> expectedChildren = new HashMap<>();
        expectedChildren.put(1, Arrays.asList(2, 3, 4));
        expectedChildren.put(2, Arrays.asList(5, 6));
        expectedChildren.put(3, Collections.singletonList(7));
        verifyDeserializedTree(graph, res, expectedChildren);
        final Map<Integer, Event> factorGraphNodes = flattenFactorGraph(res);
        verifyLinks(graph, factorGraphNodes);
    }

    private Map<Integer, Event> flattenFactorGraph(Occurrence root) {
        final Map<Integer, Event> visited = new HashMap<>();
        visited.put(root.getId(), root);
        if (root.getChildren() != null) {
            root.getChildren().forEach(child -> traverseGraph(child, visited));
        }
        if (root.getFactors() != null) {
            root.getFactors().forEach(f -> traverseGraph(f.getEvent(), visited));
        }
        return visited;
    }

    private void traverseGraph(Event event, Map<Integer, Event> visited) {
        if (visited.containsKey(event.getId())) {
            return;
        }
        visited.put(event.getId(), event);
        if (event.getChildren() != null) {
            event.getChildren().forEach(child -> traverseGraph(child, visited));
        }
        if (event.getFactors() != null) {
            event.getFactors().forEach(f -> traverseGraph(f.getEvent(), visited));
        }
    }

    private void verifyLinks(FactorGraph graph, Map<Integer, Event> flattenedGraph) {
        final Map<Integer, Integer> referenceToId = new HashMap<>();
        graph.getNodes().forEach(dto -> referenceToId.put(dto.getReferenceId(), dto.getId()));
        for (FactorGraphEdge edge : graph.getEdges()) {
            if (edge.getLinkType().equals(HAS_PART_URI)) {
                // hasPart edges are checked in structure verification above
                continue;
            }
            final Event source = flattenedGraph.get(referenceToId.get(edge.getFrom()));
            final Event target = flattenedGraph.get(referenceToId.get(edge.getTo()));
            assertNotNull(target);
            final Optional<Factor> factor = target.getFactors().stream().filter(f ->
                    f.getTypes().contains(edge.getLinkType()) &&
                            f.getEvent().getId().equals(source.getId())).findFirst();
            assertTrue(factor.isPresent());
        }
    }

    @Test
    public void testDeserializationOfOccurrenceWithFactorsAtSameLevel() throws Exception {
        final FactorGraph graph = loadGraph("data/occurrenceWithFactorsAtSameLevel.json");
        final Occurrence res = dtoMapper.factorGraphToOccurrence(graph);
        final Map<Integer, Collection<Integer>> expectedChildren = new HashMap<>();
        expectedChildren.put(1, Collections.singletonList(8));
        expectedChildren.put(2, Arrays.asList(3, 4));
        expectedChildren.put(5, Arrays.asList(6, 7));
        verifyDeserializedTree(graph, res, expectedChildren);
        final Map<Integer, Event> factorGraphNodes = flattenFactorGraph(res);
        verifyLinks(graph, factorGraphNodes);
    }

    private FactorGraph loadGraph(String fileName) throws Exception {
        return Environment.loadData(fileName, FactorGraph.class);
    }
}
