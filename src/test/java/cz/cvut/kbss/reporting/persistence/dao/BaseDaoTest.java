/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.persistence.dao;

import cz.cvut.kbss.reporting.environment.util.Generator;
import cz.cvut.kbss.reporting.model.Person;
import cz.cvut.kbss.reporting.persistence.BaseDaoTestRunner;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class BaseDaoTest extends BaseDaoTestRunner {

    @Autowired
    private PersonDao personDao;

    @Test
    public void existsForExistingInstanceReturnsTrue() throws Exception {
        final Person person = Generator.getPerson();
        personDao.persist(person);
        assertTrue(personDao.exists(person.getId()));
    }

    @Test
    public void findAllReturnsAllExistingInstances() {
        final List<Person> instances = generateInstances();
        personDao.persist(instances);
        final List<Person> result = personDao.findAll();

        assertEquals(instances.size(), result.size());
        boolean found = false;
        for (Person p : instances) {
            for (Person pp : result) {
                if (p.nameEquals(pp)) {
                    found = true;
                    break;
                }
            }
            assertTrue(found);
        }
    }

    private List<Person> generateInstances() {
        final List<Person> instances = new ArrayList<>();
        for (int i = 0; i < Generator.randomInt(10); i++) {
            final Person p = new Person();
            p.setFirstName("user" + i);
            p.setLastName("lastName" + i);
            p.setUsername("user" + i + "@kbss.felk.cvut.cz");
            p.setPassword("kb33" + i);
            instances.add(p);
        }
        return instances;
    }

    @Test
    public void existsReturnsFalseForNullId() {
        assertFalse(personDao.exists(null));
    }

    @Test
    public void removeCollectionRemovesEveryInstanceInIt() {
        final List<Person> persons = generateInstances();
        personDao.persist(persons);

        personDao.remove(persons);
        persons.forEach(p -> assertNull(personDao.find(p.getId())));
    }

    @Test
    public void removeEmptyCollectionDoesNothing() {
        final List<Person> persons = generateInstances();
        personDao.persist(persons);

        personDao.remove(Collections.emptyList());
        persons.forEach(p -> assertNotNull(personDao.find(p.getId())));
    }
}
