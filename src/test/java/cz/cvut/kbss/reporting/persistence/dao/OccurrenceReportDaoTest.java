/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.persistence.dao;

import cz.cvut.kbss.reporting.dto.ReportRevisionInfo;
import cz.cvut.kbss.reporting.environment.util.Environment;
import cz.cvut.kbss.reporting.environment.util.Generator;
import cz.cvut.kbss.reporting.model.*;
import cz.cvut.kbss.reporting.persistence.BaseDaoTestRunner;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

import static org.junit.Assert.*;

public class OccurrenceReportDaoTest extends BaseDaoTestRunner {

    private static final String ORGANIZATION_NAME = "Czech Technical University in Prague";

    @Autowired
    private OccurrenceReportDao occurrenceReportDao;

    @Autowired
    private OccurrenceDao occurrenceDao;

    @Autowired
    private OrganizationDao organizationDao;

    @PersistenceContext
    private EntityManager em;

    private Person author;

    @Before
    public void setUp() throws Exception {
        this.author = Generator.getPerson();
        persistPerson(author);
    }

    @Test
    public void persistNewReportPersistsOccurrenceAsWell() {
        final OccurrenceReport report = persistReport();

        final Occurrence occurrence = occurrenceDao.find(report.getOccurrence().getId());
        assertNotNull(occurrence);
    }

    private OccurrenceReport persistReport() {
        final OccurrenceReport report = Generator.generateOccurrenceReport(true);
        report.setAuthor(author);
        occurrenceReportDao.persist(report);
        return report;
    }

    @Test
    public void persistReportWithFactorGraphCascadesPersistToAppropriateEventInstances() {
        final OccurrenceReport report = Generator.generateOccurrenceReportWithFactorGraph();
        report.setAuthor(author);
        occurrenceReportDao.persist(report);

        final OccurrenceReport res = occurrenceReportDao.find(report.getId());
        verifyFactorGraph(report.getOccurrence(), res.getOccurrence());
    }

    private void verifyFactorGraph(Occurrence expected, Occurrence actual) {
        assertEquals(expected.getId(), actual.getId());
        if (expected.getChildren() != null) {
            assertEquals(expected.getChildren().size(), actual.getChildren().size());
        }
        if (expected.getFactors() != null) {
            assertEquals(expected.getFactors().size(), actual.getFactors().size());
        }
        final Set<Integer> visited = new HashSet<>();
        visited.add(actual.getId());
        verifyChildren(expected.getChildren(), actual.getChildren(), visited);
        verifyFactors(expected.getFactors(), actual.getFactors(), visited);
    }

    private void verifyChildren(List<Event> expected, List<Event> actual, Set<Integer> visited) {
        final Iterator<Event> itExp = expected.iterator();
        final Iterator<Event> itAct = actual.iterator();
        while (itExp.hasNext() && itAct.hasNext()) {
            verifyFactorGraph(itExp.next(), itAct.next(), visited);
        }
    }

    private void verifyFactors(Set<Factor> expected, Set<Factor> actual, Set<Integer> visited) {
        final List<Factor> lExpected = new ArrayList<>(expected);
        Collections.sort(lExpected, (a, b) -> a.getId().compareTo(b.getId()));
        final List<Factor> lActual = new ArrayList<>(actual);
        Collections.sort(lActual, (a, b) -> a.getId().compareTo(b.getId()));
        final Iterator<Factor> itExp = lExpected.iterator();
        final Iterator<Factor> itAct = lActual.iterator();
        while (itExp.hasNext() && itAct.hasNext()) {
            verifyFactorGraph(itExp.next().getEvent(), itAct.next().getEvent(), visited);
        }
    }

    private void verifyFactorGraph(Event expected, Event actual, Set<Integer> visited) {
        if (visited.contains(actual.getId())) {
            return;
        }
        visited.add(actual.getId());
        assertEquals(expected.getId(), actual.getId());
        if (expected.getChildren() != null) {
            if (expected.getChildren().isEmpty()) {
                assertTrue(actual.getChildren() == null || actual.getChildren().isEmpty());
            } else {
                assertEquals(expected.getChildren().size(), actual.getChildren().size());
                verifyChildren(expected.getChildren(), actual.getChildren(), visited);
            }
        }
        if (expected.getFactors() != null) {
            if (expected.getFactors().isEmpty()) {
                assertTrue(actual.getFactors() == null || actual.getFactors().isEmpty());
            } else {
                assertEquals(expected.getFactors().size(), actual.getFactors().size());
                verifyFactors(expected.getFactors(), actual.getFactors(), visited);
            }
        }
    }

    @Test
    public void findByOccurrenceGetsReportsWithMatchingOccurrence() {
        final Occurrence occurrence = Generator.generateOccurrence();
        occurrenceDao.persist(occurrence);
        final List<OccurrenceReport> reports = persistReportsForOccurrence(occurrence);
        // This one is just so that the method does not simply select all reports
        persistReport();

        final List<OccurrenceReport> result = occurrenceReportDao.findByOccurrence(occurrence);
        assertTrue(Environment.areEqual(reports, result));
    }

    private List<OccurrenceReport> persistReportsForOccurrence(Occurrence occurrence) {
        final List<OccurrenceReport> reports = new ArrayList<>();
        for (int i = 0; i < Generator.randomInt(10); i++) {
            final OccurrenceReport r = Generator.generateOccurrenceReport(true);
            r.setOccurrence(occurrence);
            r.setAuthor(author);
            reports.add(r);
        }
        occurrenceReportDao.persist(reports);
        return reports;
    }

    @Test
    public void findByOccurrenceReturnsLatestRevisionsOfMatchingReportChains() throws Exception {
        final Occurrence occurrence = Generator.generateOccurrence();
        occurrenceDao.persist(occurrence);
        final Set<Integer> reportIds = new HashSet<>();
        for (int i = 0; i < Generator.randomInt(10); i++) {
            final List<OccurrenceReport> chain = Generator.generateOccurrenceReportChain(author, false);
            chain.forEach(r -> r.setOccurrence(occurrence));
            occurrenceReportDao.persist(chain);
            reportIds.add(chain.get(chain.size() - 1).getId());
        }

        final List<OccurrenceReport> result = occurrenceReportDao.findByOccurrence(occurrence);
        assertEquals(reportIds.size(), result.size());
        result.forEach(r -> assertTrue(reportIds.contains(r.getId())));
    }

    @Test
    public void findByOccurrenceReturnsReportsOrderedByOccurrenceStartDescending() {
        final Occurrence occurrence = Generator.generateOccurrence();
        occurrenceDao.persist(occurrence);
        final List<OccurrenceReport> reports = persistReportsForOccurrence(occurrence);
        Collections.sort(reports,
                (a, b) -> b.getOccurrence().getStartTime().compareTo(a.getOccurrence().getStartTime()));  // Descending

        final List<OccurrenceReport> result = occurrenceReportDao.findByOccurrence(occurrence);
        assertTrue(Environment.areEqual(reports, result));
    }

    @Test
    public void persistPersistsReportWithCorrectiveMeasureRequestsWithResponsibleAgentsAndRelatedOccurrence() {
        final OccurrenceReport report = prepareReportWithMeasureRequests();
        occurrenceReportDao.persist(report);

        final OccurrenceReport result = occurrenceReportDao.find(report.getId());
        assertNotNull(result);
        Environment.areEqual(report.getCorrectiveMeasures(), result.getCorrectiveMeasures());
    }

    private OccurrenceReport prepareReportWithMeasureRequests() {
        final OccurrenceReport report = Generator.generateOccurrenceReport(true);
        report.setAuthor(author);
        final Organization org = new Organization(ORGANIZATION_NAME);
        report.setCorrectiveMeasures(new HashSet<>());
        organizationDao.persist(org);   // The organization must exist
        for (int i = 0; i < Generator.randomInt(10); i++) {
            final CorrectiveMeasureRequest req = new CorrectiveMeasureRequest();
            req.setDescription("Corrective measure request " + i);
            if (i % 2 == 0) {
                req.setResponsibleAgents(Collections.singletonList(author));
            } else {
                req.setResponsibleAgents(Collections.singletonList(org));
            }
            req.setBasedOn(report.getOccurrence());
            report.getCorrectiveMeasures().add(req);
        }
        return report;
    }

    @Test
    public void updateUpdatesCorrectiveMeasureRequestsInReport() {
        final OccurrenceReport report = prepareReportWithMeasureRequests();
        occurrenceReportDao.persist(report);

        final CorrectiveMeasureRequest newRequest = new CorrectiveMeasureRequest();
        newRequest.setDescription("Added corrective measure request");
        newRequest.setResponsibleAgents(Arrays.asList(author, organizationDao.findByName(ORGANIZATION_NAME)));
        final Iterator<CorrectiveMeasureRequest> it = report.getCorrectiveMeasures().iterator();
        it.next();
        it.remove();
        report.getCorrectiveMeasures().add(newRequest);
        final OccurrenceReport expected = occurrenceReportDao.update(report);

        final OccurrenceReport result = occurrenceReportDao.find(report.getId());
        Environment.areEqual(expected.getCorrectiveMeasures(), result.getCorrectiveMeasures());
        verifyOrphanRemoval(report);
    }

    private void verifyOrphanRemoval(OccurrenceReport report) {
        final Long cnt = em
                .createQuery("SELECT count(m) FROM " + CorrectiveMeasureRequest.class.getSimpleName() + " m",
                        Long.class)
                .getSingleResult();
        assertEquals(report.getCorrectiveMeasures().size(), cnt.intValue());
    }

    @Test
    public void updateWorksForReportsWithoutCorrectiveMeasures() {
        final OccurrenceReport report = persistReport();

        report.setSummary("New updated summary.");
        occurrenceReportDao.update(report);

        final OccurrenceReport result = occurrenceReportDao.find(report.getId());
        assertEquals(report.getSummary(), result.getSummary());
    }

    @Test
    public void reportUpdateCascadesChangeToOccurrence() {
        final OccurrenceReport report = persistReport();

        final String newName = "UpdatedOccurrenceName";
        report.getOccurrence().setName(newName);
        occurrenceReportDao.update(report);

        final OccurrenceReport result = occurrenceReportDao.find(report.getId());
        assertEquals(newName, result.getOccurrence().getName());
    }

    @Test
    public void updateReportByAddingItemsIntoFactorGraph() {
        final OccurrenceReport report = Generator.generateOccurrenceReportWithFactorGraph();
        report.setAuthor(author);
        occurrenceReportDao.persist(report);

        final Event addedOne = new Event();
        addedOne.setStartTime(report.getOccurrence().getStartTime());
        addedOne.setEndTime(report.getOccurrence().getEndTime());
        addedOne.setEventType(Generator.generateEventType());
        final Factor newF = new Factor();
        newF.setEvent(addedOne);
        newF.addType(Generator.randomFactorType());
        report.getOccurrence().addFactor(newF);
        final Event addedChild = new Event();
        addedChild.setStartTime(report.getOccurrence().getStartTime());
        addedChild.setEndTime(report.getOccurrence().getEndTime());
        addedChild.setEventType(Generator.generateEventType());
        report.getOccurrence().getChildren().iterator().next().addChild(addedChild);

        final OccurrenceReport expected = occurrenceReportDao.update(report);

        final OccurrenceReport result = occurrenceReportDao.find(report.getId());
        verifyFactorGraph(expected.getOccurrence(), result.getOccurrence());
    }

    @Test
    public void updateReportByRemovingItemsFromFactorGraph() {
        final OccurrenceReport report = Generator.generateOccurrenceReportWithFactorGraph();
        report.setAuthor(author);
        occurrenceReportDao.persist(report);

        final Iterator<Event> evtRemove = report.getOccurrence().getChildren().iterator().next().getChildren()
                                                .iterator();
        evtRemove.next();
        evtRemove.remove();

        final Iterator<Event> factRemove = report.getOccurrence().getFactors().iterator().next().getEvent()
                                                 .getChildren().iterator();
        factRemove.next();
        factRemove.remove();

        final OccurrenceReport expected = occurrenceReportDao.update(report);

        final OccurrenceReport result = occurrenceReportDao.find(report.getId());
        verifyFactorGraph(expected.getOccurrence(), result.getOccurrence());
    }

    @Test
    public void removeDeletesCorrectiveMeasuresAsWell() {
        final OccurrenceReport report = prepareReportWithMeasureRequests();
        occurrenceReportDao.persist(report);

        occurrenceReportDao.remove(report);
        assertFalse(occurrenceReportDao.exists(report.getId()));
        for (CorrectiveMeasureRequest cmr : report.getCorrectiveMeasures()) {
            assertNull(em.find(CorrectiveMeasureRequest.class, cmr.getId()));
        }
    }

    @Test
    public void getReportChainRevisionsReturnsAllRevisions() {
        final List<OccurrenceReport> chain = Generator.generateOccurrenceReportChain(author, false);
        occurrenceReportDao.persist(chain);
        Collections.reverse(chain); // Make it descending by revision number

        final List<ReportRevisionInfo> revisions =
                occurrenceReportDao.getReportChainRevisions(chain.get(0).getFileNumber());
        assertEquals(chain.size(), revisions.size());
        for (int i = 0; i < chain.size(); i++) {
            assertEquals(chain.get(i).getId(), revisions.get(i).getId());
            assertEquals(chain.get(i).getRevision(), revisions.get(i).getRevision());
        }
    }
}
