/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.persistence.dao;

import cz.cvut.kbss.reporting.environment.util.Generator;
import cz.cvut.kbss.reporting.environment.util.TestUtils;
import cz.cvut.kbss.reporting.model.Event;
import cz.cvut.kbss.reporting.model.Factor;
import cz.cvut.kbss.reporting.model.Occurrence;
import cz.cvut.kbss.reporting.model.qam.Answer;
import cz.cvut.kbss.reporting.model.qam.Question;
import cz.cvut.kbss.reporting.persistence.BaseDaoTestRunner;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class OccurrenceDaoTest extends BaseDaoTestRunner {

    private static final int MAX_DEPTH = 5;

    @Autowired
    private OccurrenceDao dao;

    @PersistenceContext
    private EntityManager em;

    @Test
    public void persistPersistsAllEventsFromFactorGraph() {
        final Occurrence occurrence = Generator.generateOccurrence();
        final List<Event> events = new ArrayList<>();
        generateFactorGraph(occurrence, events, 0);
        dao.persist(occurrence);

        final Occurrence res = dao.find(occurrence.getId());
        assertNotNull(res);
        for (Event e : events) {
            assertNotNull(em.find(Event.class, e.getId()));
        }
    }

    private void generateFactorGraph(Occurrence occurrence, List<Event> events, int depth) {
        for (int i = 0; i < Generator.randomInt(5); i++) {
            final Event evt = event(events);
            occurrence.addChild(evt);
            generateFactorGraph(evt, events, depth + 1);
        }
        for (int i = 0; i < Generator.randomInt(5); i++) {
            final Factor f = factor(events);
            occurrence.addFactor(f);
            generateFactorGraph(f.getEvent(), events, depth + 1);
        }
    }

    private Event event(List<Event> events) {
        final Event evt = new Event();
        evt.setStartTime(new Date());
        evt.setEndTime(new Date());
        evt.setEventType(Generator.generateEventType());
        events.add(evt);
        return evt;
    }

    private Factor factor(List<Event> events) {
        final Factor f = new Factor();
        f.setEvent(event(events));
        f.addType(Generator.randomFactorType());
        return f;
    }

    private void generateFactorGraph(Event event, List<Event> events, int depth) {
        if (depth == MAX_DEPTH) {
            return;
        }
        for (int i = 0; i < Generator.randomInt(5); i++) {
            final Event evt = event(events);
            event.addChild(evt);
            generateFactorGraph(evt, events, depth + 1);
        }
        for (int i = 0; i < Generator.randomInt(5); i++) {
            final Factor f = factor(events);
            event.addFactor(f);
            generateFactorGraph(f.getEvent(), events, depth + 1);
        }
    }

    @Test
    public void persistPersistsQuestionsAndAnswersOfEvents() {
        final Occurrence occurrence = Generator.generateOccurrence();
        occurrence.setQuestion(Generator.generateQuestions(null));
        dao.persist(occurrence);
        TestUtils.verifyQuestions(occurrence.getQuestion(), question -> {
            final Question res = em.find(Question.class, question.getId());
            assertNotNull(res);
            final Set<Integer> childIds = question.getSubQuestions().stream().map(Question::getId)
                                                  .collect(Collectors.toSet());
            Assert.assertEquals(question.getSubQuestions().size(), res.getSubQuestions().size());
            res.getSubQuestions().forEach(sq -> assertTrue(childIds.contains(sq.getId())));
            Assert.assertEquals(question.getAnswers().size(), res.getAnswers().size());
            // Assuming only one answer, the string representation can be used for comparison
            Assert.assertEquals(question.getAnswers().iterator().next().toString(),
                    res.getAnswers().iterator().next().toString());
        });
    }

    @Test
    public void persistReusesQuestionsWithTheSameId() {
        final Occurrence occurrence = Generator.generateOccurrence();
        final List<Event> children = new ArrayList<>(2);
        occurrence.setChildren(children);
        event(children);
        final Event evt = children.iterator().next();
        evt.setQuestion(generateReusedQuestions());
        dao.persist(occurrence);
        TestUtils.verifyQuestions(evt.getQuestion(), q -> assertNotNull(em.find(Question.class, q.getId())));
    }

    private Question generateReusedQuestions() {
        final Question root = Generator.question();
        Question copy = null;
        for (int i = 0; i < 5; i++) {
            copy = Generator.question();
            root.getSubQuestions().add(copy);
        }
        // Copy the first one into the last one to simulate behaviour when multiple question instances (received from the UI)
        // may represent the same one
        final Question first = root.getSubQuestions().iterator().next();
        copy.setId(first.getId());
        return root;
    }

    @Test
    public void removeDeletesAllQuestionsAndAnswersAsWell() {
        final Occurrence occurrence = Generator.generateOccurrence();
        occurrence.setQuestion(Generator.generateQuestions(null));
        dao.persist(occurrence);

        dao.remove(occurrence);
        TestUtils.verifyQuestions(occurrence.getQuestion(), question -> {
            assertNull(em.find(Question.class, question.getId()));
            question.getAnswers().forEach(a -> assertNull(em.find(Answer.class, a.getId())));
        });
    }
}
