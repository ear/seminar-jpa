/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.model;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PersonTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private Person person;

    @Before
    public void setUp() {
        this.person = new Person();
    }

    @Test
    public void encodePasswordThrowsIllegalStateForNullPassword() {
        thrown.expect(IllegalStateException.class);
        thrown.expectMessage("Cannot encode an empty password.");
        person.encodePassword(new StandardPasswordEncoder());
    }

    @Test
    public void nameEqualsReturnsFalseForNullOtherPerson() {
        person.setFirstName("a");
        person.setLastName("b");
        assertFalse(person.nameEquals(null));
    }

    @Test
    public void testNameEquals() {
        person.setFirstName("a");
        person.setLastName("b");
        final Person other = new Person();
        other.setFirstName("a");
        other.setLastName("b");
        assertTrue(person.nameEquals(other));
        assertTrue(other.nameEquals(person));
    }

    @Test
    public void nameEqualsTestsBothFirstAndLastName() {
        person.setFirstName("a");
        person.setLastName("b");
        final Person other = new Person();
        other.setFirstName("a");
        other.setLastName("c");
        assertFalse(person.nameEquals(other));
        other.setLastName("b");
        other.setFirstName("c");
        assertFalse(person.nameEquals(other));
    }
}