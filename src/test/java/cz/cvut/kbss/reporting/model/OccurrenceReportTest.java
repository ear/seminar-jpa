/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.model;

import cz.cvut.kbss.reporting.dto.reportlist.OccurrenceReportDto;
import cz.cvut.kbss.reporting.dto.reportlist.ReportDto;
import cz.cvut.kbss.reporting.environment.util.Generator;
import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class OccurrenceReportTest {

    @Test
    public void copyConstructorCopiesRelevantAttributes() {
        final OccurrenceReport original = Generator.generateOccurrenceReport(true);
        final OccurrenceReport copy = new OccurrenceReport(original);

        assertNull(copy.getDateCreated());
        assertNull(copy.getAuthor());
        assertNull(copy.getLastModified());
        assertNull(copy.getLastModifiedBy());

        assertEquals(original.getFileNumber(), copy.getFileNumber());
        assertEquals(original.getPhase(), copy.getPhase());
        assertEquals(original.getOccurrence().getName(), copy.getOccurrence().getName());
        assertEquals(original.getSummary(), copy.getSummary());
        assertEquals(original.getSeverityAssessment(), copy.getSeverityAssessment());
    }

    @Test
    public void copyConstructorCreatesNewCorrectiveMeasureRequests() {
        final OccurrenceReport original = Generator.generateOccurrenceReport(true);
        final Set<CorrectiveMeasureRequest> requests = new HashSet<>();
        final CorrectiveMeasureRequest rOne = new CorrectiveMeasureRequest();
        rOne.setDescription("CorrectiveMeasureRequest_One");
        requests.add(rOne);
        final CorrectiveMeasureRequest rTwo = new CorrectiveMeasureRequest();
        rTwo.setDescription("CorrectiveMeasureRequest_Two");
        rTwo.setResponsibleAgents(Collections.singletonList(Generator.getPerson()));
        requests.add(rTwo);
        original.setCorrectiveMeasures(requests);

        final OccurrenceReport copy = new OccurrenceReport(original);
        assertNotNull(copy.getCorrectiveMeasures());
        assertEquals(original.getCorrectiveMeasures().size(), copy.getCorrectiveMeasures().size());
        for (CorrectiveMeasureRequest r : original.getCorrectiveMeasures()) {
            for (CorrectiveMeasureRequest rr : copy.getCorrectiveMeasures()) {
                assertNotSame(r, rr);
            }
        }
    }

    @Test
    public void testToReportDto() {
        final OccurrenceReport report = Generator.generateOccurrenceReport(true);
        report.setPhase(Generator.generateEventType());

        final ReportDto dto = report.toReportDto();
        assertTrue(dto instanceof OccurrenceReportDto);
        final OccurrenceReportDto result = (OccurrenceReportDto) dto;
        assertEquals(report.getFileNumber(), result.getFileNumber());
        assertEquals(report.getPhase(), result.getPhase());
        assertEquals(report.getAuthor(), result.getAuthor());
        assertEquals(report.getDateCreated(), result.getDateCreated());
        assertEquals(report.getLastModified(), result.getLastModified());
        assertEquals(report.getLastModifiedBy(), result.getLastModifiedBy());
        assertEquals(report.getRevision(), result.getRevision());
        assertEquals(report.getOccurrence().getName(), result.getIdentification());
        assertEquals(report.getOccurrence().getStartTime(), result.getDate());
        assertEquals(report.getSeverityAssessment(), result.getSeverityAssessment());
        assertEquals(report.getOccurrence().getEventType(), result.getOccurrenceCategory());
        assertEquals(report.getSummary(), result.getSummary());
    }
}
