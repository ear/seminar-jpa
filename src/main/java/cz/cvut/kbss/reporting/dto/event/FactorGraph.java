/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.dto.event;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import cz.cvut.kbss.reporting.model.Event;

import java.util.List;
import java.util.Set;

@JsonPropertyOrder(value = {"nodes", "edges"})
public class FactorGraph {

    private List<Event> nodes;

    private Set<FactorGraphEdge> edges;

    public List<Event> getNodes() {
        return nodes;
    }

    public void setNodes(List<Event> nodes) {
        this.nodes = nodes;
    }

    public Set<FactorGraphEdge> getEdges() {
        return edges;
    }

    public void setEdges(Set<FactorGraphEdge> edges) {
        this.edges = edges;
    }
}
