/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.model.util.factorgraph.clone;

import cz.cvut.kbss.reporting.model.Event;
import cz.cvut.kbss.reporting.model.Occurrence;
import cz.cvut.kbss.reporting.model.util.factorgraph.FactorGraphNodeVisitor;

import java.util.Map;

public class NodeCloningVisitor implements FactorGraphNodeVisitor {

    private final Map<Integer, Event> instanceMap;

    public NodeCloningVisitor(Map<Integer, Event> instanceMap) {
        this.instanceMap = instanceMap;
    }

    @Override
    public void visit(Occurrence occurrence) {
        if (!instanceMap.containsKey(occurrence.getId())) {
            final Occurrence clone = new Occurrence(occurrence);
            instanceMap.put(occurrence.getId(), clone);
        }
    }

    @Override
    public void visit(Event event) {
        if (!instanceMap.containsKey(event.getId())) {
            final Event clone = new Event(event);
            instanceMap.put(event.getId(), clone);
        }
    }
}
