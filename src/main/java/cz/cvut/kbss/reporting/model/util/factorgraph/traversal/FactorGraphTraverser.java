/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.model.util.factorgraph.traversal;

import cz.cvut.kbss.reporting.model.Event;
import cz.cvut.kbss.reporting.model.Occurrence;
import cz.cvut.kbss.reporting.model.Vocabulary;
import cz.cvut.kbss.reporting.model.util.factorgraph.FactorGraphEdgeVisitor;
import cz.cvut.kbss.reporting.model.util.factorgraph.FactorGraphNodeVisitor;

import java.util.HashSet;
import java.util.Set;

/**
 * Traverses the factor graph, using the specified visitors on corresponding nodes.
 */
public class FactorGraphTraverser {

    private FactorGraphNodeVisitor nodeVisitor;
    private FactorGraphEdgeVisitor factorGraphEdgeVisitor;

    public FactorGraphTraverser(FactorGraphNodeVisitor nodeVisitor, FactorGraphEdgeVisitor factorGraphEdgeVisitor) {
        this.nodeVisitor = nodeVisitor;
        this.factorGraphEdgeVisitor = factorGraphEdgeVisitor;
    }

    public void setNodeVisitor(FactorGraphNodeVisitor nodeVisitor) {
        this.nodeVisitor = nodeVisitor;
    }

    public void setFactorGraphEdgeVisitor(FactorGraphEdgeVisitor factorGraphEdgeVisitor) {
        this.factorGraphEdgeVisitor = factorGraphEdgeVisitor;
    }

    /**
     * Traverses factor graph rooted in the specified {@link Occurrence}.
     *
     * @param root Factor graph root
     */
    public void traverse(Occurrence root) {
        final Set<Integer> visited = new HashSet<>();
        if (nodeVisitor != null) {
            root.accept(nodeVisitor);
        }
        visited.add(root.getId());
        if (root.getChildren() != null) {
            for (Event e : root.getChildren()) {
                if (factorGraphEdgeVisitor != null) {
                    factorGraphEdgeVisitor.visit(root, e, Vocabulary.s_p_has_part);
                }
                traverse(e, visited);
            }
        }
        traverseFactors(root, visited);
    }

    private void traverseFactors(Event item, Set<Integer> visited) {
        if (item.getFactors() != null) {
            item.getFactors().forEach(f -> {
                if (factorGraphEdgeVisitor != null) {
                    // Assuming there is exactly one factor type
                    assert f.getTypes().size() == 1;
                    factorGraphEdgeVisitor.visit(f.getEvent(), item, f.getTypes().iterator().next());
                }
                traverse(f.getEvent(), visited);
            });
        }
    }

    private void traverse(Event event, Set<Integer> visited) {
        if (visited.contains(event.getId())) {
            return;
        }
        if (nodeVisitor != null) {
            event.accept(nodeVisitor);
        }
        visited.add(event.getId());
        if (event.getChildren() != null) {
            event.getChildren().forEach(e -> {
                if (factorGraphEdgeVisitor != null) {
                    factorGraphEdgeVisitor.visit(event, e, Vocabulary.s_p_has_part);
                }
                traverse(e, visited);
            });
        }
        traverseFactors(event, visited);
    }
}
