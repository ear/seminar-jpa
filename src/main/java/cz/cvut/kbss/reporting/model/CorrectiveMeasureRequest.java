/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class CorrectiveMeasureRequest extends AbstractEntity implements Serializable {

    private String description;

    @JoinTable(name = "CORRECTIVE_MEASURE_AGENT",
               joinColumns = @JoinColumn(name = "MEASURE_ID", referencedColumnName = "ID"),
               inverseJoinColumns = @JoinColumn(name = "AGENT_ID", referencedColumnName = "ID"))
    private List<Agent> responsibleAgents;

    @JoinColumn(name = "EVENT_ID")
    private Event basedOn;

    public CorrectiveMeasureRequest() {
    }

    /**
     * Copy constructor.
     * <p>
     * Responsible agents are reused.
     *
     * @param other The instance to copy
     */
    public CorrectiveMeasureRequest(CorrectiveMeasureRequest other) {
        Objects.requireNonNull(other);
        this.description = other.description;
        if (other.responsibleAgents != null) {
            this.responsibleAgents = new ArrayList<>(other.responsibleAgents);
        }
        this.basedOn = other.basedOn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Agent> getResponsibleAgents() {
        return responsibleAgents;
    }

    public void setResponsibleAgents(List<Agent> responsibleAgents) {
        this.responsibleAgents = responsibleAgents;
    }

    public Event getBasedOn() {
        return basedOn;
    }

    public void setBasedOn(Event basedOn) {
        this.basedOn = basedOn;
    }

    @Override
    public String toString() {
        // First 50 characters of the description
        if (description != null) {
            return "CorrectiveMeasureRequest{" +
                    (description.length() > 50 ? description.substring(0, 50) + "..." : description) + '}';
        }
        return "CorrectiveMeasureRequest{" + getId() + "}";
    }
}
