/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.model.util.factorgraph.clone;

import cz.cvut.kbss.reporting.model.Event;
import cz.cvut.kbss.reporting.model.Factor;
import cz.cvut.kbss.reporting.model.Vocabulary;
import cz.cvut.kbss.reporting.model.util.factorgraph.FactorGraphEdgeVisitor;

import java.util.Map;

public class EdgeCloningVisitor implements FactorGraphEdgeVisitor {

    private static final String HAS_PART_URI = Vocabulary.s_p_has_part;

    private final Map<Integer, Event> instanceMap;

    public EdgeCloningVisitor(Map<Integer, Event> instanceMap) {
        this.instanceMap = instanceMap;
    }

    @Override
    public void visit(Event from, Event to, String type) {
        final Event source = instanceMap.get(from.getId());
        assert source != null;
        final Event target = instanceMap.get(to.getId());
        assert target != null;
        if (type.equals(HAS_PART_URI)) {
            source.addChild((Event) target);
        } else {
            final Factor factor = new Factor();
            factor.setEvent((Event) source);
            factor.addType(type);
            target.addFactor(factor);
        }
    }
}
