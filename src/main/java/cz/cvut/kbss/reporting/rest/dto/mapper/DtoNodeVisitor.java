/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.rest.dto.mapper;

import cz.cvut.kbss.reporting.model.Event;
import cz.cvut.kbss.reporting.model.Occurrence;
import cz.cvut.kbss.reporting.model.util.factorgraph.FactorGraphNodeVisitor;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SplittableRandom;

class DtoNodeVisitor implements FactorGraphNodeVisitor {

    private final Map<Integer, Event> instanceMap = new LinkedHashMap<>();

    private final SplittableRandom random;

    DtoNodeVisitor(SplittableRandom random) {
        this.random = random;
    }

    private void generateReferenceId(Integer id) {
        final Event dto = instanceMap.get(id);
        if (dto.getReferenceId() == null) {
            dto.setReferenceId(random.nextInt());
        }
    }

    @Override
    public void visit(Occurrence occurrence) {
        if (!instanceMap.containsKey(occurrence.getId())) {
            instanceMap.put(occurrence.getId(), occurrence);
        }
        generateReferenceId(occurrence.getId());
    }

    @Override
    public void visit(Event event) {
        if (!instanceMap.containsKey(event.getId())) {
            instanceMap.put(event.getId(), event);
        }
        generateReferenceId(event.getId());
    }


    Map<Integer, Event> getInstanceMap() {
        return instanceMap;
    }
}
