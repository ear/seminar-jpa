/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.rest.dto.mapper;

import cz.cvut.kbss.reporting.dto.OccurrenceReportDto;
import cz.cvut.kbss.reporting.dto.event.FactorGraph;
import cz.cvut.kbss.reporting.dto.event.FactorGraphEdge;
import cz.cvut.kbss.reporting.model.*;
import cz.cvut.kbss.reporting.model.util.factorgraph.traversal.FactorGraphTraverser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.*;

@Mapper(componentModel = "spring", uses = {ReferenceMapper.class})
public abstract class DtoMapper {

    private final SplittableRandom random = new SplittableRandom();

    private static final Map<Class<?>, Class<?>> mappedClasses = initMappedClasses();

    // Don't forget to add the classes here when adding new mapping methods
    private static Map<Class<?>, Class<?>> initMappedClasses() {
        final Map<Class<?>, Class<?>> map = new HashMap<>();
        map.put(OccurrenceReport.class, OccurrenceReportDto.class);
        map.put(OccurrenceReportDto.class, OccurrenceReport.class);
        map.put(Occurrence.class, FactorGraph.class);
        map.put(FactorGraph.class, Occurrence.class);
        return map;
    }

    /**
     * Returns true if the specified classes can be mapped by this mapper.
     *
     * @param cls The class to map
     * @return Whether the class can be mapped
     */
    public boolean canMap(Class<?> cls) {
        return mappedClasses.containsKey(cls);
    }

    @Mapping(source = "occurrence", target = "occurrence")
    @Mapping(source = "occurrence", target = "factorGraph", dependsOn = "occurrence")
    public abstract OccurrenceReportDto occurrenceReportToOccurrenceReportDto(OccurrenceReport report);

    @Mapping(source = "factorGraph", target = "occurrence")
    public abstract OccurrenceReport occurrenceReportDtoToOccurrenceReport(OccurrenceReportDto dto);

    public FactorGraph occurrenceToFactorGraph(Occurrence occurrence) {
        if (occurrence == null) {
            return null;
        }
        final DtoNodeVisitor nodeVisitor = new DtoNodeVisitor(random);
        final FactorGraphTraverser traverser = new FactorGraphTraverser(nodeVisitor, null);
        traverser.traverse(occurrence);
        final DtoEdgeVisitor edgeVisitor = new DtoEdgeVisitor();
        traverser.setFactorGraphEdgeVisitor(edgeVisitor);
        traverser.traverse(occurrence);
        final FactorGraph graph = new FactorGraph();
        graph.setNodes(new ArrayList<>(nodeVisitor.getInstanceMap().values()));
        graph.setEdges(edgeVisitor.getEdges());
        return graph;
    }

    public Occurrence factorGraphToOccurrence(FactorGraph graph) {
        if (graph == null) {
            return null;
        }
        final Map<Integer, Event> instanceMap = new HashMap<>();
        graph.getNodes().forEach(n -> instanceMap.put(n.getReferenceId(), n));
        transformEdgesToRelations(graph, instanceMap);
        final Optional<Event> occurrence = instanceMap.values().stream()
                                                      .filter(item -> item instanceof Occurrence).findFirst();
        assert occurrence.isPresent();
        return (Occurrence) occurrence.get();
    }

    private void transformEdgesToRelations(FactorGraph graph,
                                           Map<Integer, Event> instanceMap) {
        for (FactorGraphEdge e : graph.getEdges()) {
            final Event source = instanceMap.get(e.getFrom());
            final Event target = instanceMap.get(e.getTo());
            if (e.getLinkType().equals(Vocabulary.s_p_has_part)) {
                source.addChild(target);
            } else {
                final Factor factor = new Factor();
                factor.addType(e.getLinkType());
                factor.setEvent(source);
                target.addFactor(factor);
            }
        }
    }
}
