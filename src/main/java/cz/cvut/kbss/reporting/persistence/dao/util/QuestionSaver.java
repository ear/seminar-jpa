/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.persistence.dao.util;

import cz.cvut.kbss.reporting.model.qam.Question;

import javax.persistence.EntityManager;
import java.util.HashSet;
import java.util.Set;

/**
 * Recursively persists questions, handling references to the same question instance (they have the same id).
 */
public class QuestionSaver {

    private Set<Integer> visited = new HashSet<>();

    public void persistIfNecessary(Question root, EntityManager em) {
        if (visited.contains(root.getId())) {
            return;
        }
        persist(root, em);
        visited.add(root.getId());
        root.getSubQuestions().forEach(q -> this.persistSubQuestionIfNecessary(q, em));
    }

    private void persist(Question question, EntityManager em) {
        em.persist(question);
    }

    private void persistSubQuestionIfNecessary(Question question, EntityManager em) {
        if (visited.contains(question.getId())) {
            return;
        }
        persist(question, em);
        visited.add(question.getId());
        question.getSubQuestions().forEach(q -> persistSubQuestionIfNecessary(q, em));
    }
}
