/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.persistence.dao;

import cz.cvut.kbss.reporting.model.Event;
import cz.cvut.kbss.reporting.model.Factor;
import cz.cvut.kbss.reporting.model.Occurrence;
import cz.cvut.kbss.reporting.persistence.dao.util.QuestionSaver;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class OccurrenceDao extends BaseDao<Occurrence> {

    private QuestionSaver questionSaver;

    public OccurrenceDao() {
        super(Occurrence.class);
    }

    @Transactional
    @Override
    public void persist(Occurrence entity) {
        assert entity != null;
        persistEventsIfNecessary(entity);
        em.persist(entity);
    }

    private void persistEventsIfNecessary(Occurrence entity) {
        final Map<Event, Object> visited = new IdentityHashMap<>();
        this.questionSaver = new QuestionSaver();
        if (entity.getQuestion() != null) {
            questionSaver.persistIfNecessary(entity.getQuestion(), em);
        }
        if (entity.getChildren() != null) {
            entity.getChildren().forEach(e -> persistEventIfNecessary(e, visited));
        }
        if (entity.getFactors() != null) {
            entity.getFactors().forEach(f -> persistEventIfNecessary(f.getEvent(), visited));
        }
    }

    private void persistEventIfNecessary(Event event, Map<Event, Object> visited) {
        if (visited.containsKey(event)) {
            return;
        }
        visited.put(event, null);
        if (event.getChildren() != null) {
            event.getChildren().forEach(e -> persistEventIfNecessary(e, visited));
        }
        if (event.getFactors() != null) {
            event.getFactors().forEach(f -> persistEventIfNecessary(f.getEvent(), visited));
        }
        if (event.getId() == null) {
            em.persist(event);
            if (event.getQuestion() != null) {
                questionSaver.persistIfNecessary(event.getQuestion(), em);
            }
        }
    }

    @Transactional
    @Override
    public Occurrence update(Occurrence entity) {
        final Occurrence original = em.find(Occurrence.class, entity.getId());
        removeOrphans(original, entity);
        persistEventsIfNecessary(entity);
        return em.merge(entity);
    }

    private void removeOrphans(Occurrence original, Occurrence update) {
        final Set<Event> visited = new HashSet<>();
        removeOrphans(original.getChildren(), update.getChildren(), visited);
        removeOrphansFromFactors(original.getFactors(), update.getFactors(), visited);
    }

    private void removeOrphans(List<Event> original, List<Event> actual, Set<Event> visited) {
        if (original == null || original.isEmpty()) {
            return;
        }
        final Map<Integer, Event> actualIds = new HashMap<>();
        if (actual != null) {
            actual.forEach(e -> actualIds.put(e.getId(), e));
        }
        final Set<Event> toRemove = original.stream().filter(e -> !actualIds.containsKey(e.getId()))
                                            .collect(Collectors.toSet());
        toRemove.forEach(em::remove);
        original.removeAll(toRemove);
        for (Event e : original) {
            removeOrphans(e, actualIds.get(e.getId()), visited);
        }
    }

    private void removeOrphans(Event original, Event update, Set<Event> visited) {
        if (visited.contains(original)) {
            return;
        }
        visited.add(original);
        if (original.getChildren() != null) {
            removeOrphans(original.getChildren(), update.getChildren(), visited);
        }
        removeOrphansFromFactors(original.getFactors(), update.getFactors(), visited);
    }

    private void removeOrphansFromFactors(Set<Factor> original, Set<Factor> update, Set<Event> visited) {
        if (original != null) {
            for (Factor of : original) {
                final Optional<Factor> af = update == null ? Optional.empty() :
                        update.stream()
                              .filter(f -> f.getId() != null && f.getId().equals(of.getId()))
                              .findFirst();
                if (af.isPresent()) {
                    removeOrphans(of.getEvent(), af.get().getEvent(), visited);
                }
            }
        }
    }
}
