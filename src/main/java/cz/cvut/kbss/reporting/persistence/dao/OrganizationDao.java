/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.persistence.dao;

import cz.cvut.kbss.reporting.model.Organization;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Repository
public class OrganizationDao extends BaseDao<Organization> {

    public OrganizationDao() {
        super(Organization.class);
    }

    /**
     * Gets organization with the specified name.
     *
     * @param name Organization name
     * @return Organization or {@code null}
     */
    @Transactional(readOnly = true)
    public Organization findByName(String name) {
        if (name == null) {
            return null;
        }
        try {
            return em.createQuery("SELECT o FROM Organization o WHERE o.name=:name", Organization.class)
                     .setParameter("name", name).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
