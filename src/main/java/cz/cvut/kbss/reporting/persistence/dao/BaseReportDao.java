/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.reporting.persistence.dao;

import cz.cvut.kbss.reporting.dto.ReportRevisionInfo;
import cz.cvut.kbss.reporting.exception.PersistenceException;
import cz.cvut.kbss.reporting.model.LogicalDocument;
import cz.cvut.kbss.reporting.model.OccurrenceReport;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import java.util.List;
import java.util.Objects;

abstract class BaseReportDao<T extends LogicalDocument> extends BaseDao<T> {

    BaseReportDao(Class<T> type) {
        super(type);
    }

    @Transactional(readOnly = true)
    @Override
    public List<T> findAll() {
        return em.createQuery("SELECT r FROM " + type.getSimpleName() + " r WHERE " +
                "r.revision = (SELECT MAX(rr.revision) FROM " + type
                .getSimpleName() + " rr WHERE r.fileNumber = rr.fileNumber)" +
                " ORDER BY r.occurrence.startTime DESC, r.revision DESC", type)
                 .getResultList();
    }

    /**
     * Gets latest revision in report chain with the specified file number.
     * <p>
     * The returned report is latest revision of type managed by this DAO. It does not mean that there cannot be a newer
     * revision of different type.
     *
     * @param fileNumber Report chain identifier
     * @return Report with highest revision number or {@code null} if there is no such with the specified file number
     */
    @Transactional(readOnly = true)
    public T findLatestRevision(Long fileNumber) {
        try {
            return em.createQuery("SELECT r FROM " + type.getSimpleName() + " r WHERE " +
                    "r.fileNumber = :fileNumber AND " +
                    "r.revision = (SELECT MAX(rr.revision) FROM " + type
                    .getSimpleName() + " rr WHERE r.fileNumber = rr.fileNumber)" +
                    " ORDER BY r.occurrence.startTime DESC, r.revision DESC", type)
                     .setParameter("fileNumber", fileNumber).getSingleResult();
        } catch (NoResultException e) {
            return null;
        } catch (NonUniqueResultException e) {
            LOG.error("Expected single report with the highest revision number, but got multiple!");
            throw new PersistenceException(e);
        }
    }

    /**
     * Finds report with the specified revision in report chain with the specified identifier.
     *
     * @param fileNumber Report chain identifier
     * @param revision   Report revision
     * @return Matching report or {@code null}, if no such report exists
     */
    @Transactional(readOnly = true)
    public T findRevision(Long fileNumber, Integer revision) {
        try {
            return em.createQuery(
                    "SELECT r FROM " + type
                            .getSimpleName() + " r WHERE r.fileNumber = :fileNumber AND r.revision = :revision", type)
                     .setParameter("fileNumber", fileNumber).setParameter("revision", revision).getSingleResult();
        } catch (NoResultException e) {
            return null;
        } catch (NonUniqueResultException e) {
            LOG.error("Expected single report the specified revision number, but got multiple!");
            throw new PersistenceException(e);
        }
    }

    /**
     * Removes all reports in report chain with the specified identifier.
     * <p>
     * Does nothing if there is no such report chain
     *
     * @param fileNumber Report chain identifier
     */
    @Transactional
    public void removeReportChain(Long fileNumber) {
        Objects.requireNonNull(fileNumber);
        final List<T> chain = loadChain(fileNumber);
        if (chain.isEmpty()) {
            return;
        }
        // Better call the remove method instead of directly remove on EM in case there is some additional remove logic
        chain.forEach(this::remove);
    }

    private List<T> loadChain(Long fileNumber) {
        return em.createQuery("SELECT r FROM " + type.getSimpleName() + " r WHERE r.fileNumber=:fileNumber", type)
                 .setParameter("fileNumber", fileNumber).getResultList();
    }

    /**
     * Gets a list of revision info instances for a report chain identified by the specified file number.
     *
     * @param fileNumber Report chain identifier
     * @return List of revision info objects, ordered by revision number (descending)
     */
    @Transactional(readOnly = true)
    public List<ReportRevisionInfo> getReportChainRevisions(Long fileNumber) {
        Objects.requireNonNull(fileNumber);
        return em.createQuery(
                "SELECT new " + ReportRevisionInfo.class
                        .getCanonicalName() + "(r.id, r.dateCreated, r.revision) FROM " + OccurrenceReport.class
                        .getSimpleName() + " r WHERE r.fileNumber = :fileNumber ORDER BY r.revision DESC",
                ReportRevisionInfo.class).setParameter("fileNumber", fileNumber).getResultList();
    }
}
